function loadJsFile(url) {
    if (!checkIfScriptExists(url)) {
        dynamicallyLoadScript(url);
    } else {
    }
}

function dynamicallyLoadScript(url) {
    var script = document.createElement("script");
    script.src = url;
    script.async = true;
    document.head.appendChild(script);
}
function checkIfScriptExists(url) {
    var scripts = document.querySelectorAll('script');
    for (var i = 0; i < scripts.length; i++) {
        if (scripts[i].src.replace(/http(s)*:/, "") == url.replace(/http(s)*:/, "")) {
            return true;
        }
    }
    return false;
}
function loadCSS(url) {
    var cssId = 'myCss';
    if (!document.getElementById(cssId))
    {
        var head  = document.getElementsByTagName('head')[0];
        var link  = document.createElement('link');
        link.id   = cssId;
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = url;
        link.media = 'all';
        head.appendChild(link);
    }
}



if (typeof window.THEOplayer !== 'undefined' ) {
} else {
    loadCSS('//cdn.theoplayer.com/dash/theoplayer/ui.css');
    loadJsFile('//cdn.theoplayer.com/dash/theoplayer/THEOplayer.js');
    window.addEventListener('theoplayerready', function () {
        loadJsFile('http://localhost:8080/examples/theoplayerify.js');
    });
}
function handleMessage(event) {
    if (event.data.type) {
        chrome.runtime.sendMessage({type: event.data.type, text: event.data.text}, function (response) {
            console.log(response.farewell);
        });
    }
}
window.addEventListener("message", function(event) {
    // We only accept messages from ourselves
    if (event.source != window)
        return;
    handleMessage(event);
});