// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/**
 * Get the current URL.
 *
 * @param {function(string)} callback called when the URL of the current tab
 *   is found.
 */
function getCurrentTabUrl(callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, (tabs) => {
    var tab = tabs[0];

    var url = tab.url;
    console.assert(typeof url == 'string', 'tab.url should be a string');

    callback(url);
  });
}

/**
 * Change the background color of the current page.
 *
 * @param {string} color The new background color.
 */
function executeScript(script) {
  chrome.tabs.executeScript({
    // code: script
    code: script
  });
}


// This extension loads the saved background color for the current tab if one
// exists. The user can select a new background color from the dropdown for the
// current page, and it will be saved as part of the extension's isolated
// storage. The chrome.storage API is used for this purpose. This is different
// from the window.localStorage API, which is synchronous and stores data bound
// to a document's origin. Also, using chrome.storage.sync instead of
// chrome.storage.local allows the extension data to be synced across multiple
// user devices.
document.addEventListener('DOMContentLoaded', () => {
  getCurrentTabUrl((url) => {
    var dropdown = document.getElementById('dropdown');
    var button = document.querySelector('.go');
    button.addEventListener('click', function(e) {
      console.log("click happened", window.THEOplayer);
    })
  });

function handleRequest(request) {
  if (request.type = 'activequalitychanged') {
    var activeQuality = document.querySelector('.theoplayer-info .active-quality span');
    if (activeQuality) {
      activeQuality.innerHTML = request.text;
    }
  }
}

// only arrives when inspecting extension
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
      console.log(request);
      handleRequest(request);
      sendResponse({farewell: request.type + " successfully updated!"});
    });
});