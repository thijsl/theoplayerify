console.log("THEOPLAYERIFY ALIVE!", THEOplayer);
var id = 0, elements = [], player;
function createTHEODiv(video) {
    var div = document.createElement('div');
    div.style.width = video.style.width;
    div.style.height = video.style.height;
    div.id = "theoplayer-"+id;
    id++;
    div.className = "video-js theoplayer-skin theo-seekbar-above-controls";
    div.style.background = "black";
    elements.push(div);
    insertAfter(div, video);
}

function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function sendMessage(type, text) {
    var data = { type: type, text: text };
    window.postMessage(data, "*");
}

function configurePlayer(video) {
    player = new THEOplayer.Player(elements[0], { // instantiates video player
        libraryLocation : '//cdn.theoplayer.com/dash/theoplayer/' // references folder containing your THEOplayer library files (theoplayer.p.js, THEOplayer.js, ...)
    });

    player.videoTracks.addEventListener('addtrack', function(e0) {
        // detect quality changes of a track
        e0.track.addEventListener('activequalitychanged', function(e1) {
            sendMessage('activequalitychanged', e1.quality.bandwidth);
        });
    });

    player.source = {
        sources : [{
            src : video.currentSrc,
        }]
    };


}

var videos = document.querySelectorAll('video'),
    video = videos && videos[0];
if (video) {
    createTHEODiv(video);
    configurePlayer(video);
}
